"""
# Author(s): 'Sagan Pariyar' <saganpariyar@gmail.com>

This are utility to print some matrics on given School data set
"""
import csv
import time

# creating subset of data for different mapping, it will be handy to calculate various counts
# below dict will store city with school names set e.g {"<city1>": {"school1", "school2"} }
city_school_set = {}
# below dict will store state with city names set e.g {"<state1>": {"city1", "city2"} }
state_city_set = {}
# below dict will store metro with city names set e.g {"<metro1>": {"city1", "city2"} }
metro_city_set = {}

def add_data(dict_data, key, value):
	if key in dict_data:
		dict_data[key].add(value)
	else:
		dict_data[key] = {value}

def preprocess_data(path="school_data.csv"):
	"""
	This utility method will read the file and preprocess it in such a way that counting mechanism can be done in very efficient manner
	Inputs: 
		path: str, file path of data set
	Result: 
		It preload the set variables with all possible mappings 
	"""
	# reading the file
	with open(path, 'r', encoding='mac_roman', newline='') as file:
		reader = csv.reader(file)
		# SKIPPING the header from data set
		next(reader)
		# itterating through the file by each line 
		for row in reader:
			try:
				city = row[4]
				state = row[5]
				metro = row[8]
				school = row[3]
				# populating city_school_set
				add_data(city_school_set, city, school)
				# populating state_city_set
				add_data(state_city_set, state, city)
				# populating metro_city_set
				add_data(metro_city_set, metro, city)
			except Exception as e:
				print("Exception while reading row {0} error: {}".format(row,e))


def print_counts():
	"""
	It will print all the given counting matrics
	"""
	start = time.time()
	# counting total number of schools in given data set
	total_schools =  sum( [ len(schools) for schools in city_school_set.values() ] ) 
	print("Total schools {0}".format(total_schools))
	# couting total number of schools in each state using state_city_set
	print("Schools by State:")
	for state, cities in state_city_set.items():
		total_school = sum( [ len(city_school_set[city]) for city in cities ] ) 
		print("{0} : {1}".format(state, total_school))
	# couting total number of schools in each Metro using metro_city_set
	print("Schools by Metro-centric locale:")
	for metro, cities in metro_city_set.items():
		total_school = sum( [ len(city_school_set[city]) for city in cities ] ) 
		print("{0} : {1}".format(metro, total_school))
	# getting city which has maximum number of schools 
	total_school = 0
	max_count_city_name = "" 
	for city, schools in city_school_set.items():
		temp_len_schools = len(schools)
		if temp_len_schools > total_school:
			total_school = temp_len_schools
			max_count_city_name = city
	print("City with most schools: {0} ({1} schools)".format(max_count_city_name, total_school))
	# getting unique set of cities using city_school_set
	print("Unique cities with at least one school: {0}".format(len(city_school_set)))
	print("Time took to complete is {0} ms".format(time.time() - start))

if __name__ == '__main__':
	# calling preprocess method for precossing the data set 
	preprocess_data()
	# calling count method for printing all the count matrics
	print_counts()