"""
# Author(s): 'Sagan Pariyar' <saganpariyar@gmail.com>

This are utility to provide seaching facility on given school data set
"""

import csv
import time
from difflib import get_close_matches

# global variable for storing search key and related data as dict
# it will store school_name with city as search key and their values inside each key as dict
school_data_set_key_map = {}


def match_ratio_distance(lower_search_keys, school_search_key):
	"""
	A simple algorithm to find the number of match character between search key and data key
	Inputs: 
		lower_search_keys: list, list of words for given search key
		school_search_key: str, key from school_data_set_key_map 
	Returns:
		int, number of match charaters
	"""
	match = 0
	weights = len(lower_search_keys)
	# matching each word with school search key
	for word in lower_search_keys:
		# if word is present inside the school search key, increase the match charater by its length
		if word in school_search_key:
			match = (match + len(word)) * weights
		weights-=1
	
	return match

def preprocess_data(path="school_data.csv"):
	"""
	This utility method will read the file and preprocess it in such a way that searching mechanism can be done in very efficient manner
	Inputs: 
		path: str, file path of data set
	Result: 
		It preload the set variables with  possible search mappings 
	"""
	# reading the file
	with open(path, 'r', encoding='mac_roman', newline='') as file:
		reader = csv.reader(file)
		# SKIPPING the header from data set
		next(reader)
		# itterating through the file by each line 
		for row in reader:
			try:
				city = row[4]
				state = row[5]
				school = row[3]
				
				search_key = "{0} {1} {2}".format(school, state, city).lower()
				# populating search_key_school_map and school_data_set_token_map
				school_data_set_key_map[search_key] = [school, state, city]
			except Exception as e:
				print("Exception while reading row {0} error: {}".format(row,e))

def create_search_match_ratio_map(search_key):
	# lower the key for reducinng overheads
	lower_search_key = search_key.lower()
	# splitting the key for using each word inside data set
	lower_search_keys = lower_search_key.split(" ")
	# dict, it will store all match count for search key with each data set
	search_ration_map = {}
	search_key_length = len(lower_search_key)
	# itterating through each key in school data set
	for school_search_key, school_dict in school_data_set_key_map.items():
		# getting match count from match_ratio_distance between school search key and given search key
		match_ratio = match_ratio_distance(lower_search_keys, school_search_key)
		if match_ratio == 0:
			continue
		# store the match ratio within the map
		if match_ratio in search_ration_map:
			search_ration_map[match_ratio].append(school_search_key)
		else:
			search_ration_map[match_ratio] = [school_search_key]
	return search_ration_map

def search(search_key, number_of_records=3):
	"""
	It will search for best results from given data set
	Inputs: 
		key - str, search key
	Returns:
		top 3 school in list format
	"""
	start = time.time()
	search_ration_map = create_search_match_ratio_map(search_key)
	
	# sorting the search_ration_map for getting top n results, in descending order
	sorted_ratio = sorted(search_ration_map.keys(), reverse=True)
	print("Results for {0} (search took: {1}ms".format(search_key, time.time() - start))
	# reading the top n results
	print_index = 1
	for index in range(len(sorted_ratio)):
		match_percentage = sorted_ratio[index]
		keys = search_ration_map[sorted_ratio[index]]
		for search_key in keys:
			data = school_data_set_key_map[search_key]
			print("{0}. {1}\n{2}, {3}".format(print_index, data[0], data[2], data[1]))
			print_index+=1
			if (print_index > number_of_records):
				break
		if (print_index > number_of_records):
				break

if __name__ == '__main__':
	# calling preprocess method for loading school_data_set_maps
	preprocess_data()
	# calling the search method with search key
	search("top")	


